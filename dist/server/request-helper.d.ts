/// <reference types="node" />
import { Http2ServerRequest, IncomingHttpHeaders } from 'http2';
export declare class RequestHelper {
    protected request: Http2ServerRequest | undefined;
    protected url: URL | undefined;
    protected data: Record<string, unknown> | undefined;
    /**
     * Request helper constructor
     * @param request
     */
    init(request: Http2ServerRequest): void;
    getUrl(): URL;
    getPathname(): string;
    getHostName(): string;
    getURLSearchParams(): URLSearchParams;
    getRequest(): Http2ServerRequest | undefined;
    getHeaders(): IncomingHttpHeaders;
    getMethod(): string;
    getBodyData(): Promise<Record<string, unknown>>;
}
//# sourceMappingURL=request-helper.d.ts.map