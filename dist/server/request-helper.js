"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RequestHelper = void 0;
const defaultUrl = 'https://localhost';
class RequestHelper {
    constructor() {
        this.data = undefined;
    }
    /**
     * Request helper constructor
     * @param request
     */
    init(request) {
        var _a, _b;
        this.request = request;
        this.url = new URL(request.url, defaultUrl);
        this.data = undefined;
        let data = '';
        (_a = this.request) === null || _a === void 0 ? void 0 : _a.on('data', (chunk) => {
            data += chunk.toString();
        });
        (_b = this.request) === null || _b === void 0 ? void 0 : _b.on('end', () => {
            try {
                this.data = JSON.parse(data);
            }
            catch (e) {
                this.data = JSON.parse('{}');
            }
        });
    }
    getUrl() {
        return this.url || new URL(defaultUrl);
    }
    getPathname() {
        return this.getUrl().pathname;
    }
    getHostName() {
        var _a, _b;
        if (this.request) {
            return ((_b = (_a = this.request) === null || _a === void 0 ? void 0 : _a.headers) === null || _b === void 0 ? void 0 : _b.host) || 'localhost';
        }
        return 'localhost';
    }
    getURLSearchParams() {
        return this.getUrl().searchParams;
    }
    getRequest() {
        return this.request;
    }
    getHeaders() {
        var _a;
        return ((_a = this.request) === null || _a === void 0 ? void 0 : _a.headers) || {};
    }
    getMethod() {
        var _a;
        return ((_a = this.request) === null || _a === void 0 ? void 0 : _a.method) || '';
    }
    getBodyData() {
        return new Promise((resolve) => {
            var _a;
            if (this.data === undefined) {
                (_a = this.request) === null || _a === void 0 ? void 0 : _a.on('end', () => {
                    setTimeout(() => {
                        resolve(this.data);
                    }, 0);
                });
            }
            else {
                resolve(this.data);
            }
        });
    }
}
exports.RequestHelper = RequestHelper;
//# sourceMappingURL=request-helper.js.map