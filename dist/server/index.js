"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
const tslib_1 = require("tslib");
const http2_1 = require("http2");
const options_manager_1 = require("./options-manager");
const request_helper_1 = require("./request-helper");
const response_helper_1 = require("../response/response-helper");
class Server {
    constructor(port, keyFile, certFile) {
        this.port = port;
        this.keyFile = keyFile;
        this.certFile = certFile;
        this.optionsManager = new options_manager_1.OptionsManager();
        this.requestHelper = new request_helper_1.RequestHelper();
        this.responseHelper = new response_helper_1.ResponseHelper(this.requestHelper);
    }
    createAndListenSecureServer() {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            const options = yield this.getOptions();
            // const server = createSecureServer(options);
            // server.on('stream', (stream, requestHeaders) => {
            //   console.log('start stream');
            //   stream.respond({
            //     'Access-Control-Allow-Origin': '*',
            //     'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
            //     'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
            //     'Access-Control-Max-Age': '86400',
            //     'content-type': 'text/html',
            //     ':status': 200,
            //   });
            //   stream.end('<h1>Hi, and welcome to YLD blog!</h1>');
            //   console.log(requestHeaders);
            // });
            const server = (0, http2_1.createSecureServer)(options, this.requestHandler.bind(this));
            server.listen(this.port);
            server.on('error', (err) => global['console'].error('err', err));
            return server;
        });
    }
    getOptions() {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            return this.optionsManager.getServerOptions(this.keyFile, this.certFile);
        });
    }
    requestHandler(request, response) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            this.requestHelper.init(request);
            yield this.responseHelper.sendResponse(response);
        });
    }
}
exports.Server = Server;
//# sourceMappingURL=index.js.map