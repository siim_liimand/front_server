/// <reference types="node" />
import { SecureServerOptions } from 'http2';
export declare class OptionsManager {
    /**
     * Get server options
     * @param keyFile
     * @param certFile
     */
    getServerOptions(keyFile: string | null, certFile: string | null): Promise<SecureServerOptions>;
    /**
     * Get default response options
     */
    protected getDefaultResponseOptions(): SecureServerOptions;
    /**
     * Add key and cert
     * @param options
     * @param keyFile
     * @param certFile
     */
    protected addKeyAndCert(options: SecureServerOptions, keyFile: string | null, certFile: string | null): Promise<void>;
    /**
     * Add key
     * @param options
     * @param keyFile
     */
    protected addKey(options: SecureServerOptions, keyFile: string | null): Promise<void>;
    /**
     * Add cert
     * @param options
     * @param certFile
     */
    protected addCert(options: SecureServerOptions, certFile: string | null): Promise<void>;
    /**
     * Read file
     * @param filePath
     */
    protected readFile(filePath: string): Promise<string>;
}
//# sourceMappingURL=options-manager.d.ts.map