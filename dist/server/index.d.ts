/// <reference types="node" />
import { Http2ServerRequest, Http2ServerResponse, SecureServerOptions } from 'http2';
import { OptionsManager } from './options-manager';
import { RequestHelper } from './request-helper';
import { ResponseHelper } from '../response/response-helper';
export declare class Server {
    port: number;
    keyFile: string;
    certFile: string;
    protected optionsManager: OptionsManager;
    protected requestHelper: RequestHelper;
    protected responseHelper: ResponseHelper;
    constructor(port: number, keyFile: string, certFile: string);
    createAndListenSecureServer(): Promise<import("http2").Http2SecureServer>;
    protected getOptions(): Promise<SecureServerOptions>;
    protected requestHandler(request: Http2ServerRequest, response: Http2ServerResponse): Promise<void>;
}
//# sourceMappingURL=index.d.ts.map