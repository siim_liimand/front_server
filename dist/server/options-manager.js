"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OptionsManager = void 0;
const tslib_1 = require("tslib");
const fs_1 = require("fs");
class OptionsManager {
    /**
     * Get server options
     * @param keyFile
     * @param certFile
     */
    getServerOptions(keyFile, certFile) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            const response = this.getDefaultResponseOptions();
            yield this.addKeyAndCert(response, keyFile, certFile);
            return response;
        });
    }
    /**
     * Get default response options
     */
    getDefaultResponseOptions() {
        return {
            allowHTTP1: true,
        };
    }
    /**
     * Add key and cert
     * @param options
     * @param keyFile
     * @param certFile
     */
    addKeyAndCert(options, keyFile, certFile) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            yield this.addKey(options, keyFile);
            yield this.addCert(options, certFile);
        });
    }
    /**
     * Add key
     * @param options
     * @param keyFile
     */
    addKey(options, keyFile) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            if (keyFile) {
                options.key = yield this.readFile(keyFile);
            }
        });
    }
    /**
     * Add cert
     * @param options
     * @param certFile
     */
    addCert(options, certFile) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            if (certFile) {
                options.cert = yield this.readFile(certFile);
            }
        });
    }
    /**
     * Read file
     * @param filePath
     */
    readFile(filePath) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            return yield new Promise((resolve) => {
                (0, fs_1.readFile)(filePath, (err, data) => {
                    resolve(data.toString());
                });
            });
        });
    }
}
exports.OptionsManager = OptionsManager;
//# sourceMappingURL=options-manager.js.map