"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TemplateManager = void 0;
const tslib_1 = require("tslib");
const ejs = require('ejs');
class TemplateManager {
    constructor() {
        this.assets = [];
    }
    setAssets(assets) {
        this.assets = assets;
    }
    getTemplateHtml(component) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            const data = yield this.getViewData(component);
            const templateName = __dirname + '/../server/views/client.ejs';
            return yield new Promise((resolve, reject) => ejs.renderFile(templateName, data, {}, function (err, html) {
                if (err) {
                    return reject(JSON.stringify(err));
                }
                return resolve(html);
            }));
        });
    }
    getMetaDataComponentHtml() {
        // const element = createElement(MetaData, null, null);
        // return renderToString(element);
        return '<head><title>Leht</title></head>';
    }
    getViewData(component) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            return {
                assets: this.assets,
                component,
                metaData: this.getMetaDataComponentHtml(),
                // lang: this.pageData.languageCode,
                lang: 'et',
            };
        });
    }
}
exports.TemplateManager = TemplateManager;
//# sourceMappingURL=template-manager.js.map