/// <reference types="node" />
import { OutgoingHttpHeaders } from 'http2';
declare type Headers = OutgoingHttpHeaders;
export declare class HeadersManager {
    protected headers: Headers;
    constructor();
    addHeader(name: string, value: string): void;
    getHeaders(path: string, routeType: string): Headers;
    getErrorHeaders(): {
        'Content-Type': string;
    };
    getPageHeaders(): Headers;
    getJsHeaders(): Headers;
    getApiHeaders(): Headers;
    getImageHeaders(path: string): Headers;
    getFileHeaders(): Headers;
    protected createRobotsTxt(): Promise<void>;
    protected createManifestJson(): Promise<void>;
    protected addEtag(): void;
    protected addContentLength(): void;
}
export {};
//# sourceMappingURL=headers-manager.d.ts.map