export declare class TemplateManager {
    protected assets: string[];
    setAssets(assets: string[]): void;
    getTemplateHtml(component: string): Promise<string>;
    protected getMetaDataComponentHtml(): string;
    protected getViewData(component: string): Promise<{
        assets: string[];
        component: string;
        metaData: string;
        lang: string;
    }>;
}
//# sourceMappingURL=template-manager.d.ts.map