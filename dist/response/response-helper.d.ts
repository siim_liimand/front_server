/// <reference types="node" />
import { Http2ServerResponse, OutgoingHttpHeaders } from 'http2';
import { ApiRoute, FileRoute, ImageRoute, JsRoute } from '@dixid/front-server-router/dist/interfaces';
import { RequestHelper } from '../server/request-helper';
import { HeadersManager } from './headers-manager';
import { PageRoute } from '@dixid/front-router/dist/interfaces';
export declare class ResponseHelper {
    requestHelper: RequestHelper;
    protected route: PageRoute | JsRoute | FileRoute | ImageRoute | ApiRoute | undefined;
    protected headersManager: HeadersManager;
    constructor(requestHelper: RequestHelper);
    sendResponse(response: Http2ServerResponse): Promise<void>;
    protected getStatusCode(): number;
    protected getHeaders(): OutgoingHttpHeaders;
    protected getBodyData(): Promise<string | Buffer>;
}
//# sourceMappingURL=response-helper.d.ts.map