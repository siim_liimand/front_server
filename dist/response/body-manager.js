"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BodyManager = void 0;
const tslib_1 = require("tslib");
const preact_1 = require("preact");
const preact_render_to_string_1 = require("preact-render-to-string");
const fs_1 = (0, tslib_1.__importDefault)(require("fs"));
const template_manager_1 = require("./template-manager");
const front_services_1 = require("@dixid/front-services");
const front_server_router_1 = require("@dixid/front-server-router");
class BodyManager {
    constructor(headersManager, requestHelper) {
        this.headersManager = headersManager;
        this.requestHelper = requestHelper;
        this.isResolved = false;
        this.templateManager = new template_manager_1.TemplateManager();
    }
    getBodyData(routeType, getRouteContentCallback) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            return yield new Promise((resolve) => {
                this.resolvePageBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
                this.resolveJsBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
                this.resolveApiBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
                this.resolveImageBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
                this.resolveFileBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
                this.resolveErrorBodyData(routeType, resolve);
            });
        });
    }
    reject(routeType, resolve, error) {
        this.getErrorBodyData(routeType, resolve, error);
    }
    getAssets() {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => {
                this.readFile('/js/manifest.json', (content) => {
                    const contentStr = content.toString();
                    const json = JSON.parse(contentStr);
                    const keys = Object.keys(json);
                    const assetKeys = ['client.js', 'sw.js'];
                    const assets = keys
                        .filter((key) => assetKeys.includes(key))
                        .map((key) => json[key]);
                    resolve(assets);
                }, () => {
                    resolve([]);
                });
            });
        });
    }
    resolvePageBodyData(routeType, resolve, reject, getRouteContentCallback) {
        if (routeType === 'page') {
            this.isResolved = true;
            this.getAssets().then((assets) => {
                this.templateManager.setAssets(assets);
                this.getPageBodyData((data) => {
                    this.templateManager.getTemplateHtml(data).then((html) => {
                        this.headersManager.addHeader('Content-Length', `${html.length}`);
                        resolve(html);
                    });
                }, reject, getRouteContentCallback);
            });
        }
    }
    resolveJsBodyData(routeType, resolve, reject, getRouteContentCallback) {
        if (routeType === 'js') {
            this.isResolved = true;
            this.getJsBodyData(resolve, reject, getRouteContentCallback);
        }
    }
    resolveApiBodyData(routeType, resolve, reject, getRouteContentCallback) {
        if (routeType === 'api') {
            this.isResolved = true;
            this.getApiBodyData(resolve, reject, getRouteContentCallback);
        }
    }
    resolveImageBodyData(routeType, resolve, reject, getRouteContentCallback) {
        if (routeType === 'image') {
            this.isResolved = true;
            this.getImageBodyData(resolve, reject, getRouteContentCallback);
        }
    }
    resolveFileBodyData(routeType, resolve, reject, getRouteContentCallback) {
        if (routeType === 'file') {
            this.isResolved = true;
            this.getFileBodyData(resolve, reject, getRouteContentCallback);
        }
    }
    resolveErrorBodyData(routeType, resolve) {
        if (this.isResolved === false) {
            this.getErrorBodyData(routeType, resolve, new Error('not resolved'));
        }
    }
    readFile(fileName, resolve, reject) {
        const dirname = process.env.DIST_ROOT;
        const file = dirname + fileName;
        const bufferList = [];
        const readStream = fs_1.default.createReadStream(file);
        readStream.on('open', () => { });
        readStream.on('data', (chunk) => {
            bufferList.push(chunk);
        });
        readStream.on('end', () => {
            const buffer = Buffer.concat(bufferList);
            const data = buffer.toString();
            this.headersManager.addHeader('Content-Length', `${data.length}`);
            resolve(buffer);
        });
        readStream.on('error', (err) => {
            reject('file', resolve, err);
        });
    }
    fetch(input) {
        const url = new URL(input);
        const pathname = url.pathname;
        const route = (0, front_server_router_1.getRoute)(pathname);
        return new Promise((resolve, reject) => {
            if (route) {
                const routeType = route[2];
                const getRouteContentCallback = route[1];
                if (routeType === 'api') {
                    this.getApiBodyData((data) => {
                        resolve({
                            json: () => JSON.parse(data),
                        });
                    }, reject, getRouteContentCallback);
                }
                else {
                    reject('invalid type');
                }
            }
        });
    }
    createHtml(pageComponent, resolve, reject) {
        const siteName = '';
        const path = '';
        const query = new URLSearchParams();
        (0, front_services_1.getPageData)(this.fetch.bind(this), siteName, path, query, (pageData) => {
            const pageProps = {
                pageData,
            };
            const element = (0, preact_1.createElement)(pageComponent, pageProps);
            const content = (0, preact_render_to_string_1.render)(element);
            resolve(content);
        }, (err) => {
            reject('page', resolve, new Error(err));
        });
    }
    getApiBodyData(resolve, reject, getApiResponseCallback) {
        const getApiResponse = (apiResponse) => {
            const json = JSON.stringify(apiResponse);
            this.headersManager.addHeader('Content-Length', `${json.length}`);
            resolve(json);
        };
        this.requestHelper.getBodyData().then((data) => {
            const apiOptions = {
                data,
                method: this.requestHelper.getMethod(),
                query: this.requestHelper.getURLSearchParams().toString(),
            };
            getApiResponseCallback(getApiResponse, apiOptions);
        });
    }
    getJsBodyData(resolve, reject, getJsContentCallback) {
        const getJsContent = (fileName) => {
            this.readFile(fileName, resolve, reject);
        };
        getJsContentCallback(getJsContent);
    }
    getImageBodyData(resolve, reject, getImageResponseCallback) {
        const getImageResponse = (fileName) => {
            this.readFile(fileName, resolve, reject);
        };
        getImageResponseCallback(getImageResponse);
    }
    getFileBodyData(resolve, reject, getFileResponseCallback) {
        const getFileContent = (fileName) => {
            this.readFile(fileName, resolve, reject);
        };
        getFileResponseCallback(getFileContent);
    }
    getPageBodyData(resolve, reject, getPageComponentCallback) {
        const getPageComponent = (pageComponent) => {
            this.createHtml(pageComponent, resolve, reject);
        };
        const pageOptions = {
            method: this.requestHelper.getMethod(),
            query: this.requestHelper.getURLSearchParams().toString(),
        };
        getPageComponentCallback(getPageComponent, pageOptions);
    }
    getErrorBodyData(routeType, resolve, error) {
        resolve('Invalid route type: ' + routeType + (error ? ` error: ${error.message}` : ''));
    }
}
exports.BodyManager = BodyManager;
//# sourceMappingURL=body-manager.js.map