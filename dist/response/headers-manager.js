"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HeadersManager = void 0;
const tslib_1 = require("tslib");
class HeadersManager {
    constructor() {
        this.headers = {};
    }
    addHeader(name, value) {
        this.headers[name] = value;
    }
    getHeaders(path, routeType) {
        switch (routeType) {
            case 'page':
                return this.getPageHeaders();
            case 'js':
                return this.getJsHeaders();
            case 'api':
                return this.getApiHeaders();
            case 'file':
                return this.getFileHeaders();
            case 'image':
                return this.getImageHeaders(path);
            default:
                return this.getPageHeaders();
        }
    }
    getErrorHeaders() {
        return {
            'Content-Type': 'text/html',
        };
    }
    getPageHeaders() {
        return Object.assign({ 'Content-Type': 'text/html' }, this.headers);
    }
    getJsHeaders() {
        return Object.assign({ 'Content-Type': 'text/javascript', 'Cache-Control': 'max-age=31536000' }, this.headers);
    }
    getApiHeaders() {
        return Object.assign({ 'Content-Type': 'application/json' }, this.headers);
    }
    getImageHeaders(path) {
        let contentType = 'image/xicon'; // favicon
        if (path.endsWith('.jpg') || path.endsWith('.jpeg')) {
            contentType = 'image/jpeg';
        }
        else if (path.endsWith('.png')) {
            contentType = 'image/png';
        }
        else if (path.endsWith('.svg')) {
            contentType = 'image/svg+xml';
        }
        else if (path.endsWith('.webp')) {
            contentType = 'image/webp';
        }
        return Object.assign({ 'Access-Control-Allow-Origin': '*', 'Content-Type': contentType, 'Cache-Control': 'max-age=31536000' }, this.headers);
    }
    getFileHeaders() {
        const cspArray = [
            "connect-src 'self'",
            "default-src 'self'",
            "form-action 'self'",
            "font-src 'self'",
            "frame-src 'self'",
            "img-src 'self'",
            "manifest-src 'self'",
            "media-src 'self'",
            "object-src 'none'",
            "script-src 'self'",
            "style-src 'self'",
            "worker-src 'self'",
        ];
        const csp = cspArray.join(';');
        return Object.assign({ 'cache-control': 'no-cache, no-store, must-revalidate, pre-check=0, post-check=0', 'content-security-policy': csp, 'cross-origin-opener-policy': 'same-origin', date: new Date().toLocaleString(), etag: '', 'x-frame-options': 'DENY', 'x-xss-protection': '0' }, this.headers);
    }
    createRobotsTxt() {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            // this.headers['Content-Type'] = 'text/plain';
            // this.headers['Cache-Control'] = 'max-age=31536000';
        });
    }
    createManifestJson() {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            // this.headers['Content-Type'] = 'application/manifest+json; charset=utf-8';
            // "Cache-Control": "max-age=31536000",
            // this.respnseData = await this.getManifestJson();
        });
    }
    addEtag() {
        // this.headers['etag'] = etag(this.respnseData);
    }
    addContentLength() {
        // this.headers['content-length'] = `${this.respnseData.length}`;
    }
}
exports.HeadersManager = HeadersManager;
//# sourceMappingURL=headers-manager.js.map