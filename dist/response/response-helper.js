"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseHelper = void 0;
const tslib_1 = require("tslib");
const front_server_router_1 = require("@dixid/front-server-router");
const headers_manager_1 = require("./headers-manager");
const body_manager_1 = require("./body-manager");
class ResponseHelper {
    constructor(requestHelper) {
        this.requestHelper = requestHelper;
        this.headersManager = new headers_manager_1.HeadersManager();
    }
    sendResponse(response) {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            try {
                const path = this.requestHelper.getPathname();
                this.route = (0, front_server_router_1.getRoute)(path);
                const body = yield this.getBodyData();
                const statusCode = this.getStatusCode();
                const headers = this.getHeaders();
                response.writeHead(statusCode, headers);
                response.end(body);
            }
            catch (e) {
                const message = typeof e.message !== 'undefined' ? e.message : e;
                response.end('error: ' + message);
            }
        });
    }
    getStatusCode() {
        if (this.route) {
            return 200;
        }
        return 400;
    }
    getHeaders() {
        if (this.route) {
            const routeType = this.route[2] || 'page';
            const path = this.route[0] || '/';
            return this.headersManager.getHeaders(path, routeType);
        }
        else {
            return this.headersManager.getErrorHeaders();
        }
        return {};
    }
    getBodyData() {
        return (0, tslib_1.__awaiter)(this, void 0, void 0, function* () {
            if (this.route) {
                const getRouteContentCallback = this.route[1];
                const routeType = this.route[2] || 'page';
                const bodyManager = new body_manager_1.BodyManager(this.headersManager, this.requestHelper);
                return yield bodyManager.getBodyData(routeType, getRouteContentCallback);
            }
            return '<p>Error: Invalid url<p>';
        });
    }
}
exports.ResponseHelper = ResponseHelper;
//# sourceMappingURL=response-helper.js.map