/// <reference types="node" />
import { GetApiResponseCallback, GetFileResponseCallback, GetImageResponseCallback, GetJsContentCallback } from '@dixid/front-server-router/dist/interfaces';
import { HeadersManager } from './headers-manager';
import { TemplateManager } from './template-manager';
import { GetPageComponentCallback, PageComponent, RouteType } from '@dixid/front-router/dist/interfaces';
import { RequestHelper } from 'server/request-helper';
declare type Resolve = (data: string | Buffer) => void;
declare type Reject = (routeType: RouteType, resolve: Resolve, error?: Error) => void;
declare type AllGetRouteContentCallbacks = GetPageComponentCallback | GetApiResponseCallback | GetImageResponseCallback | GetFileResponseCallback | GetJsContentCallback;
export declare class BodyManager {
    headersManager: HeadersManager;
    requestHelper: RequestHelper;
    protected isResolved: boolean;
    protected templateManager: TemplateManager;
    constructor(headersManager: HeadersManager, requestHelper: RequestHelper);
    getBodyData(routeType: RouteType, getRouteContentCallback: AllGetRouteContentCallbacks): Promise<string | Buffer>;
    reject(routeType: RouteType, resolve: Resolve, error?: Error): void;
    getAssets(): Promise<string[]>;
    resolvePageBodyData(routeType: RouteType, resolve: Resolve, reject: Reject, getRouteContentCallback: AllGetRouteContentCallbacks): void;
    resolveJsBodyData(routeType: RouteType, resolve: Resolve, reject: Reject, getRouteContentCallback: AllGetRouteContentCallbacks): void;
    resolveApiBodyData(routeType: RouteType, resolve: Resolve, reject: Reject, getRouteContentCallback: AllGetRouteContentCallbacks): void;
    resolveImageBodyData(routeType: RouteType, resolve: Resolve, reject: Reject, getRouteContentCallback: AllGetRouteContentCallbacks): void;
    resolveFileBodyData(routeType: RouteType, resolve: Resolve, reject: Reject, getRouteContentCallback: AllGetRouteContentCallbacks): void;
    resolveErrorBodyData(routeType: RouteType, resolve: Resolve): void;
    readFile(fileName: string, resolve: Resolve, reject: Reject): void;
    fetch(input: string): Promise<unknown>;
    createHtml(pageComponent: PageComponent, resolve: Resolve, reject: Reject): void;
    getApiBodyData(resolve: Resolve, reject: Reject, getApiResponseCallback: GetApiResponseCallback): void;
    getJsBodyData(resolve: Resolve, reject: Reject, getJsContentCallback: GetJsContentCallback): void;
    getImageBodyData(resolve: Resolve, reject: Reject, getImageResponseCallback: GetImageResponseCallback): void;
    getFileBodyData(resolve: Resolve, reject: Reject, getFileResponseCallback: GetFileResponseCallback): void;
    getPageBodyData(resolve: Resolve, reject: Reject, getPageComponentCallback: GetPageComponentCallback): void;
    getErrorBodyData(routeType: RouteType, resolve: Resolve, error?: Error): void;
}
export {};
//# sourceMappingURL=body-manager.d.ts.map