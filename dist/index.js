"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createServer = void 0;
const index_1 = require("./server/index");
const createServer = (port, keyFile, cerFile) => {
    const server = new index_1.Server(port, keyFile, cerFile);
    server.createAndListenSecureServer();
};
exports.createServer = createServer;
//# sourceMappingURL=index.js.map