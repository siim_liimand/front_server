import { Http2ServerRequest, IncomingHttpHeaders } from 'http2';

const defaultUrl = 'https://localhost';

export class RequestHelper {
  protected request: Http2ServerRequest | undefined;
  protected url: URL | undefined;
  protected data: Record<string, unknown> | undefined = undefined;

  /**
   * Request helper constructor
   * @param request
   */
  init(request: Http2ServerRequest) {
    this.request = request;
    this.url = new URL(request.url, defaultUrl);
    this.data = undefined;
    let data = '';
    this.request?.on('data', (chunk: Buffer) => {
      data += chunk.toString();
    });
    this.request?.on('end', () => {
      try {
        this.data = JSON.parse(data);
      } catch (e) {
        this.data = JSON.parse('{}');
      }
    });
  }

  public getUrl(): URL {
    return this.url || new URL(defaultUrl);
  }

  public getPathname(): string {
    return this.getUrl().pathname;
  }

  public getHostName(): string {
    if (this.request) {
      return this.request?.headers?.host || 'localhost';
    }
    return 'localhost';
  }

  public getURLSearchParams(): URLSearchParams {
    return this.getUrl().searchParams;
  }

  public getRequest(): Http2ServerRequest | undefined {
    return this.request;
  }

  public getHeaders(): IncomingHttpHeaders {
    return this.request?.headers || {};
  }

  public getMethod(): string {
    return this.request?.method || '';
  }

  public getBodyData(): Promise<Record<string, unknown>> {
    return new Promise((resolve) => {
      if (this.data === undefined) {
        this.request?.on('end', () => {
          setTimeout(() => {
            resolve(this.data as Record<string, unknown>);
          }, 0);
        });
      } else {
        resolve(this.data);
      }
    });
  }
}
