import { readFile } from 'fs';
import { SecureServerOptions } from 'http2';

type resolveOrReject = (value?: any) => void;

export class OptionsManager {
  /**
   * Get server options
   * @param keyFile
   * @param certFile
   */
  public async getServerOptions(
    keyFile: string | null,
    certFile: string | null,
  ): Promise<SecureServerOptions> {
    const response: SecureServerOptions = this.getDefaultResponseOptions();
    await this.addKeyAndCert(response, keyFile, certFile);

    return response;
  }

  /**
   * Get default response options
   */
  protected getDefaultResponseOptions(): SecureServerOptions {
    return {
      allowHTTP1: true,
    };
  }

  /**
   * Add key and cert
   * @param options
   * @param keyFile
   * @param certFile
   */
  protected async addKeyAndCert(
    options: SecureServerOptions,
    keyFile: string | null,
    certFile: string | null,
  ): Promise<void> {
    await this.addKey(options, keyFile);
    await this.addCert(options, certFile);
  }

  /**
   * Add key
   * @param options
   * @param keyFile
   */
  protected async addKey(options: SecureServerOptions, keyFile: string | null): Promise<void> {
    if (keyFile) {
      options.key = await this.readFile(keyFile);
    }
  }

  /**
   * Add cert
   * @param options
   * @param certFile
   */
  protected async addCert(options: SecureServerOptions, certFile: string | null): Promise<void> {
    if (certFile) {
      options.cert = await this.readFile(certFile);
    }
  }

  /**
   * Read file
   * @param filePath
   */
  protected async readFile(filePath: string): Promise<string> {
    return await new Promise((resolve: resolveOrReject) => {
      readFile(filePath, (err: NodeJS.ErrnoException | null, data: Buffer) => {
        resolve(data.toString());
      });
    });
  }
}
