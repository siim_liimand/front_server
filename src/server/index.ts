import {
  createSecureServer,
  Http2ServerRequest,
  Http2ServerResponse,
  SecureServerOptions,
} from 'http2';
import { OptionsManager } from './options-manager';
import { RequestHelper } from './request-helper';
import { ResponseHelper } from '../response/response-helper';

export class Server {
  protected optionsManager: OptionsManager;
  protected requestHelper: RequestHelper;
  protected responseHelper: ResponseHelper;

  constructor(public port: number, public keyFile: string, public certFile: string) {
    this.optionsManager = new OptionsManager();
    this.requestHelper = new RequestHelper();
    this.responseHelper = new ResponseHelper(this.requestHelper);
  }

  async createAndListenSecureServer() {
    const options = await this.getOptions();
    // const server = createSecureServer(options);
    // server.on('stream', (stream, requestHeaders) => {
    //   console.log('start stream');
    //   stream.respond({
    //     'Access-Control-Allow-Origin': '*',
    //     'Access-Control-Allow-Methods': 'POST, GET, OPTIONS',
    //     'Access-Control-Allow-Headers': 'X-PINGOTHER, Content-Type',
    //     'Access-Control-Max-Age': '86400',
    //     'content-type': 'text/html',
    //     ':status': 200,
    //   });
    //   stream.end('<h1>Hi, and welcome to YLD blog!</h1>');
    //   console.log(requestHeaders);
    // });
    const server = createSecureServer(options, this.requestHandler.bind(this));
    server.listen(this.port);
    server.on('error', (err) => global['console'].error('err', err));

    return server;
  }

  protected async getOptions(): Promise<SecureServerOptions> {
    return this.optionsManager.getServerOptions(this.keyFile, this.certFile);
  }

  protected async requestHandler(request: Http2ServerRequest, response: Http2ServerResponse) {
    this.requestHelper.init(request);
    await this.responseHelper.sendResponse(response);
  }
}
