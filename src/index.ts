import { Server } from './server/index';

export const createServer = (port: number, keyFile: string, cerFile: string) => {
  const server = new Server(port, keyFile, cerFile);
  server.createAndListenSecureServer();
};
