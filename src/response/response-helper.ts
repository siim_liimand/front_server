import { Http2ServerResponse, OutgoingHttpHeaders } from 'http2';
import { getRoute } from '@dixid/front-server-router';
import {
  ApiRoute,
  FileRoute,
  GetApiResponseCallback,
  GetFileResponseCallback,
  GetImageResponseCallback,
  GetJsContentCallback,
  ImageRoute,
  JsRoute,
} from '@dixid/front-server-router/dist/interfaces';
import { RequestHelper } from '../server/request-helper';
import { HeadersManager } from './headers-manager';
import { BodyManager } from './body-manager';
import {
  GetPageComponentCallback,
  PageRoute,
  RouteType,
} from '@dixid/front-router/dist/interfaces';

export class ResponseHelper {
  protected route: PageRoute | JsRoute | FileRoute | ImageRoute | ApiRoute | undefined;
  protected headersManager: HeadersManager;

  constructor(public requestHelper: RequestHelper) {
    this.headersManager = new HeadersManager();
  }

  public async sendResponse(response: Http2ServerResponse): Promise<void> {
    try {
      const path: string = this.requestHelper.getPathname();
      this.route = getRoute(path);
      const body = await this.getBodyData();
      const statusCode = this.getStatusCode();
      const headers = this.getHeaders();
      response.writeHead(statusCode, headers);
      response.end(body);
    } catch (e: unknown) {
      const message = typeof (e as Error).message !== 'undefined' ? (e as Error).message : e;
      response.end('error: ' + message);
    }
  }

  protected getStatusCode(): number {
    if (this.route) {
      return 200;
    }

    return 400;
  }

  protected getHeaders(): OutgoingHttpHeaders {
    if (this.route) {
      const routeType: RouteType = this.route[2] || 'page';
      const path: string = this.route[0] || '/';
      return this.headersManager.getHeaders(path, routeType);
    } else {
      return this.headersManager.getErrorHeaders();
    }

    return {};
  }

  protected async getBodyData(): Promise<string | Buffer> {
    if (this.route) {
      const getRouteContentCallback:
        | GetPageComponentCallback
        | GetApiResponseCallback
        | GetImageResponseCallback
        | GetFileResponseCallback
        | GetJsContentCallback = this.route[1];
      const routeType = this.route[2] || 'page';
      const bodyManager = new BodyManager(this.headersManager, this.requestHelper);

      return await bodyManager.getBodyData(routeType, getRouteContentCallback);
    }

    return '<p>Error: Invalid url<p>';
  }
}
