import {
  ApiOptions,
  GetApiResponse,
  GetApiResponseCallback,
  GetFileResponse,
  GetFileResponseCallback,
  GetImageResponse,
  GetImageResponseCallback,
  GetJsContent,
  GetJsContentCallback,
} from '@dixid/front-server-router/dist/interfaces';
import { createElement } from 'preact';
import { render } from 'preact-render-to-string';
import fs, { ReadStream } from 'fs';
import { HeadersManager } from './headers-manager';
import { TemplateManager } from './template-manager';
import { getPageData } from '@dixid/front-services';
import { PageData } from '@dixid/front-services/dist/interfaces';
import {
  GetPageComponent,
  GetPageComponentCallback,
  PageComponent,
  PageOptions,
  PageProps,
  RouteType,
} from '@dixid/front-router/dist/interfaces';
import { getRoute } from '@dixid/front-server-router';
import { RequestHelper } from 'server/request-helper';

type Resolve = (data: string | Buffer) => void;
type Reject = (routeType: RouteType, resolve: Resolve, error?: Error) => void;
type AllGetRouteContentCallbacks =
  | GetPageComponentCallback
  | GetApiResponseCallback
  | GetImageResponseCallback
  | GetFileResponseCallback
  | GetJsContentCallback;

export class BodyManager {
  protected isResolved: boolean = false;
  protected templateManager: TemplateManager;

  constructor(public headersManager: HeadersManager, public requestHelper: RequestHelper) {
    this.templateManager = new TemplateManager();
  }

  async getBodyData(
    routeType: RouteType,
    getRouteContentCallback: AllGetRouteContentCallbacks,
  ): Promise<string | Buffer> {
    return await new Promise((resolve: Resolve) => {
      this.resolvePageBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
      this.resolveJsBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
      this.resolveApiBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
      this.resolveImageBodyData(
        routeType,
        resolve,
        this.reject.bind(this),
        getRouteContentCallback,
      );
      this.resolveFileBodyData(routeType, resolve, this.reject.bind(this), getRouteContentCallback);
      this.resolveErrorBodyData(routeType, resolve);
    });
  }

  reject(routeType: RouteType, resolve: Resolve, error?: Error) {
    this.getErrorBodyData(routeType, resolve, error);
  }

  async getAssets(): Promise<string[]> {
    return new Promise((resolve) => {
      this.readFile(
        '/js/manifest.json',
        (content: string | Buffer) => {
          const contentStr = (content as Buffer).toString();
          const json: Record<string, string> = JSON.parse(contentStr);
          const keys: string[] = Object.keys(json);
          const assetKeys = ['client.js', 'sw.js'];
          const assets: string[] = keys
            .filter((key) => assetKeys.includes(key))
            .map((key): string => json[key]);
          resolve(assets);
        },
        () => {
          resolve([]);
        },
      );
    });
  }

  resolvePageBodyData(
    routeType: RouteType,
    resolve: Resolve,
    reject: Reject,
    getRouteContentCallback: AllGetRouteContentCallbacks,
  ) {
    if (routeType === 'page') {
      this.isResolved = true;
      this.getAssets().then((assets: string[]) => {
        this.templateManager.setAssets(assets);
        this.getPageBodyData(
          (data: string | Buffer) => {
            this.templateManager.getTemplateHtml(data as string).then((html) => {
              this.headersManager.addHeader('Content-Length', `${html.length}`);
              resolve(html);
            });
          },
          reject,
          getRouteContentCallback as GetPageComponentCallback,
        );
      });
    }
  }

  resolveJsBodyData(
    routeType: RouteType,
    resolve: Resolve,
    reject: Reject,
    getRouteContentCallback: AllGetRouteContentCallbacks,
  ) {
    if (routeType === 'js') {
      this.isResolved = true;
      this.getJsBodyData(resolve, reject, getRouteContentCallback as GetJsContentCallback);
    }
  }

  resolveApiBodyData(
    routeType: RouteType,
    resolve: Resolve,
    reject: Reject,
    getRouteContentCallback: AllGetRouteContentCallbacks,
  ) {
    if (routeType === 'api') {
      this.isResolved = true;
      this.getApiBodyData(resolve, reject, getRouteContentCallback as GetApiResponseCallback);
    }
  }

  resolveImageBodyData(
    routeType: RouteType,
    resolve: Resolve,
    reject: Reject,
    getRouteContentCallback: AllGetRouteContentCallbacks,
  ) {
    if (routeType === 'image') {
      this.isResolved = true;
      this.getImageBodyData(resolve, reject, getRouteContentCallback as GetImageResponseCallback);
    }
  }

  resolveFileBodyData(
    routeType: RouteType,
    resolve: Resolve,
    reject: Reject,
    getRouteContentCallback: AllGetRouteContentCallbacks,
  ) {
    if (routeType === 'file') {
      this.isResolved = true;
      this.getFileBodyData(resolve, reject, getRouteContentCallback as GetFileResponseCallback);
    }
  }

  resolveErrorBodyData(routeType: RouteType, resolve: Resolve) {
    if (this.isResolved === false) {
      this.getErrorBodyData(routeType, resolve, new Error('not resolved'));
    }
  }

  readFile(fileName: string, resolve: Resolve, reject: Reject) {
    const dirname = process.env.DIST_ROOT;
    const file = dirname + fileName;
    const bufferList: Buffer[] = [];
    const readStream: ReadStream = fs.createReadStream(file);
    readStream.on('open', () => {});
    readStream.on('data', (chunk: Buffer) => {
      bufferList.push(chunk);
    });
    readStream.on('end', () => {
      const buffer = Buffer.concat(bufferList);
      const data = buffer.toString();
      this.headersManager.addHeader('Content-Length', `${data.length}`);
      resolve(buffer);
    });
    readStream.on('error', (err: Error) => {
      reject('file', resolve, err);
    });
  }

  fetch(input: string) {
    const url = new URL(input);
    const pathname = url.pathname;
    const route = getRoute(pathname);
    return new Promise((resolve, reject) => {
      if (route) {
        const routeType = route[2];
        const getRouteContentCallback: AllGetRouteContentCallbacks = route[1];
        if (routeType === 'api') {
          this.getApiBodyData(
            (data: string | Buffer) => {
              resolve({
                json: () => JSON.parse(data as string),
              });
            },
            reject,
            getRouteContentCallback as GetApiResponseCallback,
          );
        } else {
          reject('invalid type');
        }
      }
    });
  }

  createHtml(pageComponent: PageComponent, resolve: Resolve, reject: Reject) {
    const siteName = '';
    const path = '';
    const query: URLSearchParams = new URLSearchParams();
    getPageData(
      this.fetch.bind(this),
      siteName,
      path,
      query,
      (pageData: PageData) => {
        const pageProps: PageProps = {
          pageData,
        };
        const element = createElement(pageComponent, pageProps);
        const content = render(element);
        resolve(content);
      },
      (err: string) => {
        reject('page', resolve, new Error(err));
      },
    );
  }

  getApiBodyData(resolve: Resolve, reject: Reject, getApiResponseCallback: GetApiResponseCallback) {
    const getApiResponse: GetApiResponse = (apiResponse) => {
      const json = JSON.stringify(apiResponse);
      this.headersManager.addHeader('Content-Length', `${json.length}`);
      resolve(json);
    };
    this.requestHelper.getBodyData().then((data) => {
      const apiOptions: ApiOptions = {
        data,
        method: this.requestHelper.getMethod() as 'GET',
        query: this.requestHelper.getURLSearchParams().toString(),
      };
      getApiResponseCallback(getApiResponse, apiOptions);
    });
  }

  getJsBodyData(resolve: Resolve, reject: Reject, getJsContentCallback: GetJsContentCallback) {
    const getJsContent: GetJsContent = (fileName) => {
      this.readFile(fileName, resolve, reject);
    };
    getJsContentCallback(getJsContent);
  }

  getImageBodyData(
    resolve: Resolve,
    reject: Reject,
    getImageResponseCallback: GetImageResponseCallback,
  ) {
    const getImageResponse: GetImageResponse = (fileName) => {
      this.readFile(fileName, resolve, reject);
    };
    getImageResponseCallback(getImageResponse);
  }

  getFileBodyData(
    resolve: Resolve,
    reject: Reject,
    getFileResponseCallback: GetFileResponseCallback,
  ) {
    const getFileContent: GetFileResponse = (fileName) => {
      this.readFile(fileName, resolve, reject);
    };
    getFileResponseCallback(getFileContent);
  }

  getPageBodyData(
    resolve: Resolve,
    reject: Reject,
    getPageComponentCallback: GetPageComponentCallback,
  ) {
    const getPageComponent: GetPageComponent = (pageComponent) => {
      this.createHtml(pageComponent, resolve, reject);
    };
    const pageOptions: PageOptions = {
      method: this.requestHelper.getMethod() as 'GET',
      query: this.requestHelper.getURLSearchParams().toString(),
    };
    getPageComponentCallback(getPageComponent, pageOptions);
  }

  getErrorBodyData(routeType: RouteType, resolve: Resolve, error?: Error) {
    resolve('Invalid route type: ' + routeType + (error ? ` error: ${error.message}` : ''));
  }
}
