import { OutgoingHttpHeaders } from 'http2';

type Headers = OutgoingHttpHeaders;

export class HeadersManager {
  protected headers: Headers;

  constructor() {
    this.headers = {};
  }

  addHeader(name: string, value: string) {
    this.headers[name] = value;
  }

  getHeaders(path: string, routeType: string): Headers {
    switch (routeType) {
      case 'page':
        return this.getPageHeaders();
      case 'js':
        return this.getJsHeaders();
      case 'api':
        return this.getApiHeaders();
      case 'file':
        return this.getFileHeaders();
      case 'image':
        return this.getImageHeaders(path);
      default:
        return this.getPageHeaders();
    }
  }

  getErrorHeaders() {
    return {
      'Content-Type': 'text/html',
    };
  }

  getPageHeaders(): Headers {
    return {
      'Content-Type': 'text/html',
      ...this.headers,
    };
  }

  getJsHeaders(): Headers {
    return {
      'Content-Type': 'text/javascript',
      'Cache-Control': 'max-age=31536000',
      ...this.headers,
    };
  }

  getApiHeaders(): Headers {
    return {
      'Content-Type': 'application/json',
      ...this.headers,
    };
  }

  getImageHeaders(path: string): Headers {
    let contentType = 'image/xicon'; // favicon

    if (path.endsWith('.jpg') || path.endsWith('.jpeg')) {
      contentType = 'image/jpeg';
    } else if (path.endsWith('.png')) {
      contentType = 'image/png';
    } else if (path.endsWith('.svg')) {
      contentType = 'image/svg+xml';
    } else if (path.endsWith('.webp')) {
      contentType = 'image/webp';
    }

    return {
      'Access-Control-Allow-Origin': '*',
      'Content-Type': contentType,
      'Cache-Control': 'max-age=31536000',
      ...this.headers,
    };
  }

  getFileHeaders(): Headers {
    const cspArray = [
      "connect-src 'self'",
      "default-src 'self'",
      "form-action 'self'",
      "font-src 'self'",
      "frame-src 'self'",
      "img-src 'self'",
      "manifest-src 'self'",
      "media-src 'self'",
      "object-src 'none'",
      "script-src 'self'",
      "style-src 'self'",
      "worker-src 'self'",
    ];
    const csp = cspArray.join(';');
    return {
      'cache-control': 'no-cache, no-store, must-revalidate, pre-check=0, post-check=0',
      'content-security-policy': csp,
      'cross-origin-opener-policy': 'same-origin',
      date: new Date().toLocaleString(),
      etag: '',
      'x-frame-options': 'DENY',
      'x-xss-protection': '0',
      ...this.headers,
    };
  }

  protected async createRobotsTxt(): Promise<void> {
    // this.headers['Content-Type'] = 'text/plain';
    // this.headers['Cache-Control'] = 'max-age=31536000';
  }

  protected async createManifestJson(): Promise<void> {
    // this.headers['Content-Type'] = 'application/manifest+json; charset=utf-8';
    // "Cache-Control": "max-age=31536000",
    // this.respnseData = await this.getManifestJson();
  }

  protected addEtag(): void {
    // this.headers['etag'] = etag(this.respnseData);
  }

  protected addContentLength(): void {
    // this.headers['content-length'] = `${this.respnseData.length}`;
  }
}
