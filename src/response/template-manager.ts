const ejs = require('ejs');

export class TemplateManager {
  protected assets: string[] = [];

  public setAssets(assets: string[]) {
    this.assets = assets;
  }

  async getTemplateHtml(component: string): Promise<string> {
    const data = await this.getViewData(component);
    const templateName = __dirname + '/../server/views/client.ejs';
    return await new Promise((resolve, reject) =>
      ejs.renderFile(templateName, data, {}, function (err: any, html: string) {
        if (err) {
          return reject(JSON.stringify(err));
        }
        return resolve(html);
      }),
    );
  }

  protected getMetaDataComponentHtml(): string {
    // const element = createElement(MetaData, null, null);
    // return renderToString(element);
    return '<head><title>Leht</title></head>';
  }

  protected async getViewData(component: string): Promise<{
    assets: string[];
    component: string;
    metaData: string;
    lang: string;
  }> {
    return {
      assets: this.assets,
      component,
      metaData: this.getMetaDataComponentHtml(),
      // lang: this.pageData.languageCode,
      lang: 'et',
    };
  }
}
