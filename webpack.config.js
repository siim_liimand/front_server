/* eslint-disable no-undef */

const nodeExternals = require('webpack-node-externals');
const path = require('path');
const CopyPlugin = require('copy-webpack-plugin');
// const BundleAnalyzerPlugin = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');

module.exports = {
  name: 'server',
  entry: {
    index: path.resolve(__dirname, 'src/index.ts'),
  },
  mode: 'production',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    uniqueName: 'fd',
    chunkLoadingGlobal: 'wpfd',
  },
  externals: [nodeExternals()],
  resolve: {
    extensions: ['.ts', '.tsx'],
    alias: {
      '@actions': path.resolve(__dirname, 'src/actions'),
      '@components': path.resolve(__dirname, 'src/components'),
      '@constants': path.resolve(__dirname, 'src/constants'),
      '@routes': path.resolve(__dirname, 'src/routes'),
      '@services': path.resolve(__dirname, 'src/services'),
      '@config': path.resolve(__dirname, 'src/config'),
      '@pages': path.resolve(__dirname, 'src/pages'),
      '@client': path.resolve(__dirname, 'src/client'),
      '@hooks': path.resolve(__dirname, 'src/hooks'),
      '@context': path.resolve(__dirname, 'src/context-providers'),
      '@utils': path.resolve(__dirname, 'src/utils'),
      '@public': path.resolve(__dirname, 'src/public'),
      '@lib': path.resolve(__dirname, 'src/lib'),
    },
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          configFile: 'tsconfig.json',
          transpileOnly: true,
        },
      },
      {
        test: /favicon\.ico$/i,
        loader: 'file-loader',
        options: {
          name: '/favicon.ico',
        },
      },
      {
        test: /\/images\/icons\/[a-z0-9_-]+\.(png|jpe?g|svg|ico|webp)$/i,
        loader: 'file-loader',
        options: {
          name: '/images/icons/[name].[ext]',
        },
      },
      {
        test: /\/images\/screenshots\/[a-z0-9_-]+\.(png|jpe?g|svg|ico|webp)$/i,
        loader: 'file-loader',
        options: {
          name: '/images/screenshots/[name].[ext]',
        },
      },
    ],
  },
  target: 'node',
  node: {
    __dirname: false,
  },
  plugins: [
    new CopyPlugin({
      patterns: [
        { context: 'src/server', from: 'views', to: 'views' },
        { context: 'src/public', from: 'static', to: '.' },
      ],
    }),
    // new BundleAnalyzerPlugin(),
    new ForkTsCheckerWebpackPlugin(),
  ],
};
